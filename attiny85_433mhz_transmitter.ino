#define radio_pin 0 // Select the pin where the radio is attached
#define bit_time 257 // it's actually a bit more, that's the delay time between setting a bit and setting the next
#define large_pause bit_time * 24 // not going to be used soon, this is going to be hardcoded as many zeroes

void setup() {
  pinMode(radio_pin, OUTPUT); // initialize the digital pin as an output.
  pinMode(LED_BUILTIN, OUTPUT); // initialize the digital pin as an output.
  digitalWrite(radio_pin, LOW);
  digitalWrite(LED_BUILTIN, LOW);
}

uint8_t beginning[] = {
                      0x08, 0x02, 0x0f, 0x20, 0x83, 0xcf, 0x3c, 0xf3, 0xc8, 0x3c, 0x82, 0x08, 0x20, 0xf2, 0x08, 0x3c, 
                      0x82, 0x0f, 0x20, 0x82, 0x00, 0x83, 0xc8, 0x20, 0xf3, 0xcf, 0x3c, 0xf2, 0x0f, 0x20, 0x82, 0x08, 
                      0x3c, 0x82, 0x0f, 0x20, 0x83, 0xc8, 0x20, 0x80, 0x20, 0xf2, 0x08, 0x3c, 0xf3, 0xcf, 0x3c, 0x83, 
                      0xc8, 0x20, 0x82, 0x0f, 0x20, 0x83, 0xc8, 0x20, 0xf2, 0x08, 0x20, 0x08, 0x3c, 0x82, 0x0f, 0x3c, 
                      0xf3, 0xcf, 0x20, 0xf2, 0x08, 0x20, 0x83, 0xc8, 0x20, 0xf2, 0x08, 0x3c, 0x82, 0x0f, 0xff };
uint8_t middle[] = {  0x0c, 0x3c, 0xc3, 0x0f, 0x3c, 0xf3, 0xcf, 0x30, 0xf3, 0x0c, 0x30, 0xc3, 0xcc, 0x30, 0xf3, 0x0c, 0x3c, 0xc3, 0x0f, 0xff};
uint8_t ending[] = {  0x0c, 0x3c, 0xc3, 0x0f, 0x3c, 0xf3, 0xcf, 0x30, 0xf3, 0x0c, 0x30, 0xc3, 0xcc, 0x30, 0xf3, 0x0c, 0x3c, 0xc3};

/*
char sep_a[2] = {b10000000, b00100000}; // to be sent with no modification
char one = b111100; // a logical 1 is represented by the physical sequence 111100
char a_zero = b100000; // a logical 0 is represented by either 100000 (in the first logical half of a message)
char b_zero = b110000; // or by 110000 (in the latter logical half of a message)
char preamble_a = b100; // this has to be converted by using the above rules, so the final result is 111100100000100000
char sep_b[5] = {b11111111, b11110000, b00000000, b00000000, b00000000}; // to be sent with no modification
char preamble_b = b1100; // // this has to be converted by using the above rules, so the final result is 111100111100110000110000
char plug_a_on  = "01010100001101000000"; // to be converted in a loop
char plug_a_off = "00111101101010100000"; // to be converted in a loop
// converted_message_format_a is plug_X_CMD, but each 1 is replaced by binary 111100, and each 0 is replaced by binary 100000
// converted_message_format_b is plug_X_CMD, but each 1 is replaced by binary 111100, and each 0 is replaced by binary 110000
// so, finally, a message is
// 4 times (sep_a, preamble_a, converted_message_format_a), then 4 times (sep_b, preamble_b, converted_message_format_a)
*/

void loop() {
  for (int i=0; i<sizeof(beginning)/sizeof(uint8_t); i++) {
    send_one_char(beginning[i]);
  }
  digitalWrite(radio_pin, LOW);
  digitalWrite(LED_BUILTIN, LOW);
  delayMicroseconds(large_pause);
  for (int j=0; j<3; j++) {
    for (int i=0; i<sizeof(middle)/sizeof(uint8_t); i++) {
      send_one_char(middle[i]);
    }
    digitalWrite(radio_pin, LOW);
    digitalWrite(LED_BUILTIN, LOW);
    delayMicroseconds(large_pause);
  }
  for (int i=0; i<sizeof(ending)/sizeof(uint8_t); i++) {
    send_one_char(ending[i]);
  }
  digitalWrite(radio_pin, LOW);
  digitalWrite(LED_BUILTIN, LOW);
  delayMicroseconds(large_pause);
  delay(200);
}

void send_one_char(uint8_t c) {
  for (int i=7; i>=0; i--) {
    //if ((c << i) / 128 == 1) {
    if (bitRead(c, i) == 1) {
      digitalWrite(radio_pin, HIGH);
      digitalWrite(LED_BUILTIN, HIGH);
    } else {
      digitalWrite(radio_pin, LOW);
      digitalWrite(LED_BUILTIN, LOW);
    }
    delayMicroseconds(bit_time);
  }
}
